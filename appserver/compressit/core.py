from PIL import Image
from os import path

class ImageCompressor:
	def __init__(self, image_path, codec = 'jpg', save_path = ''):
		self.set_path(image_path)
		self.set_codec(codec)
		self.set_save_path(save_path)

	def set_save_path(self, save_path):
		if save_path[-1] != '/':
			self.save_path = save_path + '/'
		else:
			self.save_path = save_path

	def set_path(self, image_path):
		self.path = image_path

	def get_name(self):
		return self.path

	def get_save_path(self):
		return self.save_path

	def make_name(self):
		return 'compressed' + '.' + self.codec

	def set_codec(self, codec = 'jpg'):
		self.codec = codec

	def convert(self, codec='jpg', quality = 70, mode = 'RGB'):
		if self.get_name() != '':
			self.set_codec(codec)
			image = Image.open(self.get_name())
			#image = Image.open('source/lena-high.jpg')
			compressed = image.convert(mode = mode, palette = 'WEB')
			compressed.save(self.get_save_path() + self.make_name(), quality = quality)
			return True
		else:
			return False

	def get_original_size(self):
		return path.getsize(self.get_name()) / 10**3

	def get_size(self):
		return path.getsize(self.get_save_path() + self.make_name()) / 10**3

	def get_compression_rate(self):
		if self.get_size() < self.get_original_size():
			return self.get_diff_size() / self.get_original_size()
		else:
			return (self.get_diff_size() * -1) / self.get_original_size() + 1

	def get_diff_size(self):
		return self.get_original_size() - self.get_size()
