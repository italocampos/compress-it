from django.apps import AppConfig


class CompressitConfig(AppConfig):
    name = 'compressit'
