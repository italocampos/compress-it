from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from compressit.core import ImageCompressor


def index(request):
	return HttpResponse("Hello, world. You're at the compressit index.")


def upload(request):
	if request.method == 'POST' and request.FILES['image']:
		image = request.FILES['image']
		quality = int(request.POST['quality'])
		codec = request.POST['codec']
		mode = request.POST['mode']

		fs = FileSystemStorage()
		filename = fs.save(image.name, image)

		compressor = ImageCompressor('media/'+image.name, save_path = 'media/')
		compressor.convert(codec, quality, mode)
		compressed_file_size = compressor.get_size()
		original_file_size = compressor.get_original_size()
		rate = compressor.get_compression_rate()

		#converted_image_url = fs.url(filename)
		converted_image_url = '/' + compressor.get_save_path() + compressor.make_name()
		return render(request, 'compressit/download_page.html', {
			'converted_image_url': converted_image_url,
			'quality': quality,
			'compression_rate': rate,
			'original_file_size': original_file_size,
			'compressed_file_size': compressed_file_size,
		})
	return render(request, 'compressit/upload_page.html')